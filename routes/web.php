<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/cast/create', 'PostController@postadd');

Route::post('/cast','PostController@store');


Route::get('/casts' ,'PostController@index');

Route::get('/cast/{cast_id}','PostController@show');

Route::get('/cast/{cast_id}/edit','PostController@edit');

Route::put('/cast/{cast_id}','PostController@Update');

Route::delete('cast/{cast_id}','PostController@delete');
