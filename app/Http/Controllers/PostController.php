<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function postadd(){
        return view('posts.postcast');
    }

    public function store(Request $request){

        $request->validate([
            "nama" => "required",
            "umur" => "required",
            "bio" => "required"
        ]);

        $query = DB::table('cast')->insert([
            "nama"=>$request["nama"],
            "umur"=>$request["umur"],
            "bio"=>$request["bio"]]);

            return redirect('/casts')->with('success','berhasil menambahkan data baru');
    }

    public function index(){
        $tampil = DB::table('cast')->get();

        return view('posts.casts', compact('tampil'));
    }


    public function show($id){
        $tampil = DB::table('cast')->where('id',$id)->first();

        return view('posts.castdetail', compact('tampil'));
    }

    public function edit($id){
        $tampil = DB::table('cast')->where('id', $id)->first();

        return view('posts.castformedit',compact('tampil'));
    }


    public function update($id , Request $request){
        $request->validate([
            "nama" => "required",
            "umur" => "required",
            "bio" => "required"
        ]);

        $tampil =DB::table('cast')
                ->where('id',$id)
                ->update([
                    "nama" => $request["nama"],
                    "umur" => $request['umur'],
                    "bio" => $request['bio']
                ]);

        return redirect('/casts')->with('success','data anda berhasil di update');

    }

    public function delete($id){
        $query = DB::table('cast')->where('id', $id)->delete();

        return redirect('/casts')->with('success','data berhasil di hapus');
    }
  
}
