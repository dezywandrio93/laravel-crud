@extends('layout.main.main')

@section('content')
<section class="content">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Input data Cast</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/cast" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" value="{{old('nama','')}}" name="nama" placeholder="Input Nama">
            </div>
            @error('nama')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
            <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" id="umur" name="umur" {{old('umur','')}} placeholder="Input Umur">
            </div>
            @error('umur')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
            <div class="form-group">
                <label for="bio">bio</label>
                <input type="text" class="form-control" id="bio" name="bio" {{old('bio','')}} placeholder="Input bio">
              </div>
          </div>
          @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
          @enderror
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
</section>

@endsection