@extends('layout.main.main')

@section('content')

    <section class="content">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Cast</h3><br>
            @if (session('success'))
                <div class="alert alert-default-info">
                    {{session('success')}}
                </div>
            @endif
              <a class="btn btn-info" href="/cast/create" >tambah</a>
            </div>
            <!-- /.card-header -->
            <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th >Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($tampil as $key => $item)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->bio}}</td>
                            <td style="">
                                <a href="/cast/{{$item->id}}" class="btn btn-success btn-sm">show</a>
                                <a href="/cast/{{$item->id}}/edit" class="btn btn-info btn-sm">update</a>
                                <form action="/cast/{{$item->id}} " method="post">
                                    @csrf
                                @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                    <p>data tidak tersedia</p>
                        
                    @endforelse
                </tbody>
              </table><div class="card-body">
              
            </div>
            <!-- /.card-body -->
            
          </div>
    </section>
    
@endsection